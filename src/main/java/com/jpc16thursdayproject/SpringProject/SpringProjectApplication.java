package com.jpc16thursdayproject.SpringProject;

import com.jpc16thursdayproject.SpringProject.dbexample.MyDBContext;
import com.jpc16thursdayproject.SpringProject.dbexample.dao.BookDAOBean;
import com.jpc16thursdayproject.SpringProject.dbexample.dao.BookDaoJDBC;
import com.jpc16thursdayproject.SpringProject.dbexample.dao.UserDAOBean;
import com.jpc16thursdayproject.SpringProject.dbexample.db.DBConnection;
import com.jpc16thursdayproject.SpringProject.dbexample.model.Book;
import com.jpc16thursdayproject.SpringProject.dbexample.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SpringProjectApplication implements CommandLineRunner {

	private BookDAOBean bookDAOBean;
	private UserDAOBean userDAOBean;

	//public SpringProjectApplication(BookDAOBean bookDAOBean) {
	//	this.bookDAOBean = bookDAOBean;
	//}

	@Autowired
	public void setBookDAOBean(BookDAOBean bookDAOBean) {
		this.bookDAOBean = bookDAOBean;
	}
	@Autowired
	public void setUserDAOBean(UserDAOBean userDAOBean) {
		this.userDAOBean = userDAOBean;
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String hashedPassword = encoder.encode("administrator");
		System.out.println(hashedPassword);
	}
}
