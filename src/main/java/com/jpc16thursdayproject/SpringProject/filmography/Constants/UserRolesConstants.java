package com.jpc16thursdayproject.SpringProject.filmography.Constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
}
