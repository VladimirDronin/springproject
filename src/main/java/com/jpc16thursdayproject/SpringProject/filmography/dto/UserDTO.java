package com.jpc16thursdayproject.SpringProject.filmography.dto;

import com.jpc16thursdayproject.SpringProject.filmography.model.Orders;
import com.jpc16thursdayproject.SpringProject.filmography.model.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends GenericDTO{
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private String email;
    private RoleDTO role;
    private List<Long> ordersIDs;
}
