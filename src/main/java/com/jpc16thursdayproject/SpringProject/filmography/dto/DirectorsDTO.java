package com.jpc16thursdayproject.SpringProject.filmography.dto;

import com.jpc16thursdayproject.SpringProject.filmography.model.Films;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@ToString
@NoArgsConstructor
@Getter
@Setter
public class DirectorsDTO extends GenericDTO {
    private String directorsFIO;
    private String position;
    List<Long> filmListIDs;
    List<FilmsDTO> filmsInfo;
}
