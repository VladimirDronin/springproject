package com.jpc16thursdayproject.SpringProject.filmography.dto;

import com.jpc16thursdayproject.SpringProject.filmography.model.Directors;
import com.jpc16thursdayproject.SpringProject.filmography.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;
@ToString
@NoArgsConstructor
@Getter
@Setter
public class FilmsDTO extends GenericDTO {
    private String filmTitle;
    private LocalDate premierYear;
    private String country;
    private Genre genre;
    List<Long> directorsIDs;
    List<DirectorsDTO> directorsInfo;
}
