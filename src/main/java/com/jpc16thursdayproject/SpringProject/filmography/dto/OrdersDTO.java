package com.jpc16thursdayproject.SpringProject.filmography.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class OrdersDTO extends GenericDTO{
    private Long usersID;
    private Long filmsID;
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
}
