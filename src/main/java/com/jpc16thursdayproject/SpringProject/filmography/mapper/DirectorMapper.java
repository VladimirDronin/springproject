package com.jpc16thursdayproject.SpringProject.filmography.mapper;

import com.jpc16thursdayproject.SpringProject.filmography.dto.DirectorsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Directors;
import com.jpc16thursdayproject.SpringProject.filmography.model.GenericModel;
import com.jpc16thursdayproject.SpringProject.filmography.repository.FilmsRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Component
public class DirectorMapper extends GenericMapper<Directors, DirectorsDTO>{
    private final FilmsRepository filmRepository;
    private final FilmMapper filmMapper;

    public DirectorMapper(ModelMapper modelMapper,
                          FilmsRepository filmRepository,
                          FilmMapper filmMapper) {
        super(Directors.class, DirectorsDTO.class, modelMapper);
        this.filmRepository = filmRepository;
        this.filmMapper = filmMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Directors.class, DirectorsDTO.class)
                .addMappings(mapping -> {
                    mapping.skip(DirectorsDTO::setFilmListIDs);
                    mapping.skip(DirectorsDTO::setFilmsInfo);
                }).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorsDTO.class, Directors.class)
                .addMappings(mapping -> mapping.skip(Directors::setFilmList)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(DirectorsDTO source, Directors destination) {
        if (!Objects.isNull(source.getFilmListIDs())) {
            destination.setFilmList(filmRepository.findAllById(source.getFilmListIDs()));
        } else {
            destination.setFilmList(Collections.emptyList());
        }

    }

    @Override
    protected void mapSpecificFields(Directors source, DirectorsDTO destination) {
        destination.setFilmListIDs(getIds(source));
        destination.setFilmsInfo(filmMapper.toDTOs(source.getFilmList()));
    }

    @Override
    protected List<Long> getIds(Directors source) {
        return Objects.isNull(source) || Objects.isNull(source.getFilmList())
                ? Collections.emptyList()
                : source.getFilmList().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

}
