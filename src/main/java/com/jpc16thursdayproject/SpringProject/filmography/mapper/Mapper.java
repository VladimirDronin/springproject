package com.jpc16thursdayproject.SpringProject.filmography.mapper;

import com.jpc16thursdayproject.SpringProject.filmography.dto.GenericDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    D toDTO(E entity);
    List<E> toEntities(List<D> dtos);
    List<D> toDTOs(List<E> entities);

}
