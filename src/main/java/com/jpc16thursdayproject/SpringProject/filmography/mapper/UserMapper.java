package com.jpc16thursdayproject.SpringProject.filmography.mapper;

import com.jpc16thursdayproject.SpringProject.filmography.dto.UserDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.GenericModel;
import com.jpc16thursdayproject.SpringProject.filmography.model.Users;
import com.jpc16thursdayproject.SpringProject.filmography.repository.OrdersRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<Users, UserDTO> {
    private OrdersRepository ordersRepository;

    public UserMapper(OrdersRepository ordersRepository, ModelMapper modelMapper){
        super(Users.class, UserDTO.class, modelMapper);
        this.ordersRepository = ordersRepository;
    }
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Users.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrdersIDs)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, Users.class)
                .addMappings(m -> m.skip(Users::setOrders)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, Users destination) {
        if (!Objects.isNull(source.getOrdersIDs())) {
            destination.setOrders(ordersRepository.findAllById(source.getOrdersIDs()));
        }
        else {
            destination.setOrders(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Users source, UserDTO destination) {
        destination.setOrdersIDs(getIds(source));
    }

    @Override
    protected List<Long> getIds(Users entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
