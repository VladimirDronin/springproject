package com.jpc16thursdayproject.SpringProject.filmography.mapper;

import com.jpc16thursdayproject.SpringProject.filmography.dto.OrdersDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Orders;
import com.jpc16thursdayproject.SpringProject.filmography.repository.FilmsRepository;
import com.jpc16thursdayproject.SpringProject.filmography.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.List;
@Component
public class OrderMapper extends GenericMapper<Orders, OrdersDTO>{
    private final FilmsRepository filmRepository;
    private final UserRepository userRepository;

    protected OrderMapper(ModelMapper mapper,
                          FilmsRepository filmRepository,
                          UserRepository userRepository) {
        super(Orders.class, OrdersDTO.class, mapper);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Orders.class, OrdersDTO.class)
                .addMappings(m -> m.skip(OrdersDTO::setUsersID))
                .addMappings(m -> m.skip(OrdersDTO::setFilmsID))
                .setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(OrdersDTO.class, Orders.class)
                .addMappings(m -> m.skip(Orders::setUsers))
                .addMappings(m -> m.skip(Orders::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrdersDTO source, Orders destination) {
        destination.setFilms(filmRepository.findById(source.getFilmsID()).orElseThrow(() -> new NotFoundException("Фильмы не найдены")));
        destination.setUsers(userRepository.findById(source.getUsersID()).orElseThrow(() -> new NotFoundException("Пользователи не найдены")));
    }

    @Override
    protected void mapSpecificFields(Orders source, OrdersDTO destination) {
        destination.setUsersID(source.getUsers().getId());
        destination.setFilmsID(source.getFilms().getId());
    }

    @Override
    protected List<Long> getIds(Orders entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

}
