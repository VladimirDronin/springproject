package com.jpc16thursdayproject.SpringProject.filmography.mapper;

import com.jpc16thursdayproject.SpringProject.filmography.dto.FilmsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Films;
import com.jpc16thursdayproject.SpringProject.filmography.model.GenericModel;
import com.jpc16thursdayproject.SpringProject.filmography.repository.DirectorsRepository;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FilmMapper extends GenericMapper<Films, FilmsDTO> {
    private final DirectorsRepository directorsRepository;
    private final DirectorMapper directorMapper;

    public FilmMapper(ModelMapper modelMapper,
                      DirectorsRepository directorsRepository,
                      DirectorMapper directorMapper) {
        super(Films.class, FilmsDTO.class, modelMapper);
        this.directorsRepository = directorsRepository;
        this.directorMapper = directorMapper;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Films.class, FilmsDTO.class)
                .addMappings(
                        mapping -> {
                            mapping.skip(FilmsDTO::setDirectorsIDs);
                            mapping.skip(FilmsDTO::setDirectorsInfo);
                        }).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmsDTO.class, Films.class)
                .addMappings(mapping -> mapping.skip(Films::setDirectors)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmsDTO source, Films destination) {
        if (!Objects.isNull(source.getDirectorsIDs())) {
            destination.setDirectors(directorsRepository.findAllById(source.getDirectorsIDs()));
        } else {
            destination.setDirectors(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Films source, FilmsDTO destination) {
        destination.setDirectorsIDs(getIds(source));
        destination.setDirectorsInfo(directorMapper.toDTOs(source.getDirectors()));
    }

    @Override
    protected List<Long> getIds(Films source) {
        return Objects.isNull(source) || Objects.isNull(source.getDirectors())
                ? Collections.emptyList()
                : source.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
