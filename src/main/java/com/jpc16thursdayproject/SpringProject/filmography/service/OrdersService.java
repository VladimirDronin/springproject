package com.jpc16thursdayproject.SpringProject.filmography.service;

import com.jpc16thursdayproject.SpringProject.filmography.dto.OrdersDTO;
import com.jpc16thursdayproject.SpringProject.filmography.mapper.OrderMapper;
import com.jpc16thursdayproject.SpringProject.filmography.model.Orders;
import com.jpc16thursdayproject.SpringProject.filmography.repository.OrdersRepository;
import org.springframework.stereotype.Service;

@Service
public class OrdersService extends GenericService<Orders, OrdersDTO>{
    protected OrdersService(OrdersRepository ordersRepository, OrderMapper orderMapper){
        super(ordersRepository, orderMapper);
    }
}
