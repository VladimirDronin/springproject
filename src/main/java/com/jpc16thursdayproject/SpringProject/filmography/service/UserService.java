package com.jpc16thursdayproject.SpringProject.filmography.service;

import com.jpc16thursdayproject.SpringProject.filmography.dto.RoleDTO;
import com.jpc16thursdayproject.SpringProject.filmography.dto.UserDTO;
import com.jpc16thursdayproject.SpringProject.filmography.mapper.UserMapper;
import com.jpc16thursdayproject.SpringProject.filmography.model.Users;
import com.jpc16thursdayproject.SpringProject.filmography.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;

public class UserService extends GenericService<Users, UserDTO>{
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    public UserService(UserRepository userRepository, UserMapper userMapper, BCryptPasswordEncoder bCryptPasswordEncoder){
        super(userRepository, userMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDTO create(UserDTO userDTO) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1l);
        userDTO.setRole(roleDTO);
        userDTO.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDTO.setCreatedBy("REGISTRATION FORM");
        userDTO.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(userDTO)));
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public boolean checkPassword(String password, UserDetails foundUser) {
        return bCryptPasswordEncoder.matches(password, foundUser.getPassword());
    }

}
