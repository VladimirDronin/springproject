package com.jpc16thursdayproject.SpringProject.filmography.service;

import com.jpc16thursdayproject.SpringProject.filmography.dto.DirectorsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.mapper.DirectorMapper;
import com.jpc16thursdayproject.SpringProject.filmography.model.Directors;
import com.jpc16thursdayproject.SpringProject.filmography.model.Films;
import com.jpc16thursdayproject.SpringProject.filmography.repository.DirectorsRepository;
import com.jpc16thursdayproject.SpringProject.filmography.repository.FilmsRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
@Service
public class DirectorService extends GenericService<Directors, DirectorsDTO>{
    private FilmsRepository filmsRepository;
    public DirectorService(DirectorsRepository directorsRepository,
                           DirectorMapper directorMapper,
                           FilmsRepository filmsRepository){
        super(directorsRepository, directorMapper);
        this.filmsRepository = filmsRepository;
    }

    public DirectorsDTO addFilm(Long filmId,
                             Long directorId) {
        Films films = filmsRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден!"));
        DirectorsDTO directors = getOne(directorId);
        directors.getFilmListIDs().add(films.getId());
        update(directors);
        return directors;
    }
}
