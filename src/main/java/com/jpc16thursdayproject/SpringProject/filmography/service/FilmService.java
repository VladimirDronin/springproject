package com.jpc16thursdayproject.SpringProject.filmography.service;

import com.jpc16thursdayproject.SpringProject.filmography.dto.DirectorsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.dto.FilmsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.mapper.FilmMapper;
import com.jpc16thursdayproject.SpringProject.filmography.model.Directors;
import com.jpc16thursdayproject.SpringProject.filmography.model.Films;
import com.jpc16thursdayproject.SpringProject.filmography.repository.DirectorsRepository;
import com.jpc16thursdayproject.SpringProject.filmography.repository.FilmsRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
@Service
public class FilmService extends GenericService<Films, FilmsDTO>{
    DirectorsRepository directorsRepository;
    public FilmService(FilmsRepository filmsRepository,
                       FilmMapper filmMapper,
                       DirectorsRepository directorsRepository){
        super(filmsRepository, filmMapper);
        this.directorsRepository = directorsRepository;
    }

    public FilmsDTO addDirector(Long filmId,
                                Long directorId) {
        Directors directors = directorsRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Директор не найден!"));
        FilmsDTO films = getOne(filmId);
        films.getDirectorsIDs().add(directors.getId());
        update(films);
        return films;
    }
}
