package com.jpc16thursdayproject.SpringProject.filmography.controller.rest;

import com.jpc16thursdayproject.SpringProject.filmography.dto.DirectorsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Directors;
import com.jpc16thursdayproject.SpringProject.filmography.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/directors")
@Tag(name = "Директоры", description = "Контроллер для работы с директорами")
public class DirectorsController extends GenericController<Directors, DirectorsDTO> {
    public DirectorsController(DirectorService directorService) {
        super(directorService);
    }

    @Operation(description = "Добавить фильм к директору")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorsDTO> addFilm(@RequestParam(value = "directorId") Long directorId,
                                          @RequestParam(value = "filmId") Long filmId) {
        return ResponseEntity.status(HttpStatus.OK).body(((DirectorService) service).addFilm(filmId, directorId));
    }
}
