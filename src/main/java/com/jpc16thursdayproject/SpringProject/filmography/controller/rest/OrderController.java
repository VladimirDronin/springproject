package com.jpc16thursdayproject.SpringProject.filmography.controller.rest;

import com.jpc16thursdayproject.SpringProject.filmography.dto.OrdersDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Orders;
import com.jpc16thursdayproject.SpringProject.filmography.service.OrdersService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class OrderController extends GenericController<Orders, OrdersDTO>{
    public OrderController(OrdersService ordersService){
        super(ordersService);
    }
}
