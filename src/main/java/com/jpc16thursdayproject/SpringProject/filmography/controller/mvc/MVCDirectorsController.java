package com.jpc16thursdayproject.SpringProject.filmography.controller.mvc;

import com.jpc16thursdayproject.SpringProject.filmography.dto.DirectorsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.dto.FilmsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.service.DirectorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/directors")
public class MVCDirectorsController {
    private final DirectorService directorService;
    public MVCDirectorsController(DirectorService directorService){
        this.directorService = directorService;
    }

    @GetMapping
    public String getAll(Model model) {
        List<DirectorsDTO> directors = directorService.listAll();
        model.addAttribute("directors", directors);
        return "films/viewAllDirectors";
    }

    @GetMapping("/add")
    public String create() {
        return "directors/addDirector";
    }
    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorsDTO newDirector) {
        directorService.create(newDirector);
        return "redirect:/films";
    }
}
