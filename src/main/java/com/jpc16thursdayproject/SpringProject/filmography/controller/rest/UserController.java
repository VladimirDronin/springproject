package com.jpc16thursdayproject.SpringProject.filmography.controller.rest;

import com.jpc16thursdayproject.SpringProject.filmography.dto.UserDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Users;
import com.jpc16thursdayproject.SpringProject.filmography.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
public class UserController extends GenericController<Users, UserDTO>{
    public UserController(UserService userService){
        super(userService);
    }
}
