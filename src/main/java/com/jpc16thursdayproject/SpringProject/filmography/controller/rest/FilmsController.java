package com.jpc16thursdayproject.SpringProject.filmography.controller.rest;

import com.jpc16thursdayproject.SpringProject.filmography.dto.FilmsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.model.Films;
import com.jpc16thursdayproject.SpringProject.filmography.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами")

public class FilmsController extends GenericController<Films, FilmsDTO> {
    public FilmsController(FilmService filmService){
        super(filmService);
    }

    @Operation(description = "Добавить директора к фильму")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmsDTO> addDirector(@RequestParam(value = "directorId") Long directorId,
                                                 @RequestParam(value = "filmId") Long filmId) {
        return ResponseEntity.status(HttpStatus.OK).body(((FilmService) service).addDirector(filmId, directorId));
    }
}
