package com.jpc16thursdayproject.SpringProject.filmography.controller.mvc;

import com.jpc16thursdayproject.SpringProject.filmography.dto.FilmsDTO;
import com.jpc16thursdayproject.SpringProject.filmography.service.FilmService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/films")
public class MVCFilmsController {
    private final FilmService filmService;
    public MVCFilmsController(FilmService filmService){
        this.filmService = filmService;
    }

    @GetMapping
    public String getAll(Model model) {
        List<FilmsDTO> films = filmService.listAll();
        model.addAttribute("films", films);
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String create() {
        return "films/addFilm";
    }
    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmsDTO newFilm) {
        filmService.create(newFilm);
        return "redirect:/films";
    }
}
