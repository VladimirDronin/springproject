package com.jpc16thursdayproject.SpringProject.filmography.repository;

import com.jpc16thursdayproject.SpringProject.filmography.model.GenericModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenericRepository<T extends GenericModel>
        extends JpaRepository<T, Long> {
}
