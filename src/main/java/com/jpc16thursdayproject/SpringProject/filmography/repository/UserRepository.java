package com.jpc16thursdayproject.SpringProject.filmography.repository;

import com.jpc16thursdayproject.SpringProject.filmography.model.Users;

public interface UserRepository
        extends GenericRepository<Users> {
    Users findUserByLogin(String login);

    Users findUserByEmail(String email);

}
