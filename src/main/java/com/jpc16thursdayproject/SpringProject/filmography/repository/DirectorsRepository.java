package com.jpc16thursdayproject.SpringProject.filmography.repository;

import com.jpc16thursdayproject.SpringProject.filmography.model.Directors;

public interface DirectorsRepository
        extends GenericRepository<Directors> {
}
