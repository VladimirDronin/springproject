package com.jpc16thursdayproject.SpringProject.filmography.repository;

import com.jpc16thursdayproject.SpringProject.filmography.model.Orders;

public interface OrdersRepository
        extends GenericRepository<Orders> {
}
