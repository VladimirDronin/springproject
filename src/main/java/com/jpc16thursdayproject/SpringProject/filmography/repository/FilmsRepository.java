package com.jpc16thursdayproject.SpringProject.filmography.repository;

import com.jpc16thursdayproject.SpringProject.filmography.model.Films;

public interface FilmsRepository
        extends GenericRepository<Films>{
}
