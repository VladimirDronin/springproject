package com.jpc16thursdayproject.SpringProject.filmography.model;

public enum Genre {
    FANTASY("Фантастика"),
    ACTION_MOVIE("Боевик"),
    DRAMA("Драма"),
    COMEDY("Комедия");

    private final String genreTextDisplay;

    Genre(String text) {
        this.genreTextDisplay = text;
    }

    public String getGenreTextDisplay() {
        return genreTextDisplay;
    }
}
