package com.jpc16thursdayproject.SpringProject.filmography.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Entity
@Table(name = "directors")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "director_sequence", allocationSize = 1)

public class Directors extends GenericModel{
    @Column(name = "directors_fio", nullable = false)
    private String directorsFIO;

    @Column(name = "position", nullable = false)
    private String position;

    @ManyToMany
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name = "directors_id"), foreignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"),
            inverseJoinColumns = @JoinColumn(name = "films_id"), inverseForeignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"))
    List<Films> filmList;

}
