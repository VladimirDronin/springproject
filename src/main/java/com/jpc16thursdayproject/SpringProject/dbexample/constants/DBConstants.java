package com.jpc16thursdayproject.SpringProject.dbexample.constants;

public interface DBConstants {
    String DB_HOST = "localhost";
    String DB = "SpringProjectDB";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
