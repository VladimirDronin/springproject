package com.jpc16thursdayproject.SpringProject.dbexample.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String lastName;
    private String firstName;
    private Integer age;
    private String phone;
    private String email;
    private List<Integer> listBooks = new ArrayList<>();

    public void setListBooks(Integer book){
        listBooks.add(book);
    }
}
