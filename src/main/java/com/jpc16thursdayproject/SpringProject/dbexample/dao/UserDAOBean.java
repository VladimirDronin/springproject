package com.jpc16thursdayproject.SpringProject.dbexample.dao;
import com.jpc16thursdayproject.SpringProject.dbexample.model.Book;
import com.jpc16thursdayproject.SpringProject.dbexample.model.User;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;

@Component
public class UserDAOBean {
    private final Connection connection;
    private final String USER_SELECT_BY_PHONE_QUERY = "select * from users where phone = ?";
    private final String USER_INSERT = "insert into Users (last_name, first_name, age, phone, email, book) " +
            "Values (?, ?, ?, ?, ?, ?)";

    public UserDAOBean(Connection connection){
        this.connection = connection;
    }

    public void createUser(String lastName, String firstName, Integer age, String phone, String email, Integer[] books) throws SQLException {
        PreparedStatement insertUser = connection.prepareStatement(USER_INSERT);
        for(Integer book : books){
            insertUser.setString(1, lastName);
            insertUser.setString(2, firstName);
            insertUser.setInt(3, age);
            insertUser.setString(4, phone);
            insertUser.setString(5, email);
            insertUser.setInt(6, book);

            insertUser.executeUpdate();
        }
    }

    public User findUser(String phone) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(USER_SELECT_BY_PHONE_QUERY);
        selectQuery.setString(1, phone);
        ResultSet resultSet = selectQuery.executeQuery();

        User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getInt("id"));
            user.setLastName(resultSet.getString("last_name"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setAge(resultSet.getInt("age"));
            user.setPhone(resultSet.getString("phone"));
            user.setEmail(resultSet.getString("email"));
            user.setListBooks(resultSet.getInt("book"));
        }
        return user;
    }
}
