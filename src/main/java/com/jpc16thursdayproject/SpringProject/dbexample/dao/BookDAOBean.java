package com.jpc16thursdayproject.SpringProject.dbexample.dao;

import com.jpc16thursdayproject.SpringProject.dbexample.model.Book;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class BookDAOBean {
    private final Connection connection;
    private final String BOOK_SELECT_BY_ID_QUERY = "select * from books where id = ?";

    public BookDAOBean(Connection connection) {
        this.connection = connection;
    }

    public List<Book> findBookById(List<Integer> bookId) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(BOOK_SELECT_BY_ID_QUERY);
        List<Book> books = new ArrayList<>();
        for(Integer i : bookId){
            Book book = new Book();
            selectQuery.setInt(1, i);
            ResultSet resultSet = selectQuery.executeQuery();
            while (resultSet.next()){
                book.setId(resultSet.getInt("id"));
                book.setAuthor(resultSet.getString("author"));
                book.setTitle(resultSet.getString("title"));
                book.setDateAdded(resultSet.getDate("date_added"));
                books.add(book);
            }
        }
        return books;
    }
}
