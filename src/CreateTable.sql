create table books
(
    id         serial primary key,
    title      varchar(100) not null,
    author     varchar(100) not null,
    date_added timestamp    not null
);


insert into books(title, author, date_added)
values ('Недоросль', 'Д. И. Фонвизин', now());
insert into books(title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
insert into books(title, author, date_added)
values ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
insert into books(title, author, date_added)
values ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());
select * from books;

create table if not exists users(
    id serial primary key ,
    last_name varchar(100) not null,
    first_name varchar(100) not null,
    age integer not null,
    phone varchar(100) not null,
    email varchar(100) not null,
    book integer references books(id) not null unique
)

delete from users;

select * from users;
